
#include <iostream>

int main(int argc, char* argv[])
{
    int num1 = 0;
    int num2 = 1;
    int next = 1;

    int sum = 0;

    while (next <= 4'000'000)
    {
        if ((next & 1) == 0)
        {
            sum += next;
        }
        next = num1 + num2;
        num1 = num2;
        num2 = next;
    }

    std::cout << sum << std::endl;

}