#pragma once


#include <cstdint>
#include <vector>
#include <cmath>

std::vector<std::uint64_t> getPrimeFactors(std::uint64_t n)
{
    std::vector<std::uint64_t> ret;

    //get all of the 2 factors
    while (n % 2 == 0)
    {
        ret.emplace_back(2);
        n /= 2;
    }

    //increment through all odd numbers up to sqrt(n)
    //composite odd numbers are taken care of because we are starting with the lowest
    //numbers, and all composite numbers must be multiples of primes already factored out
    for (std::uint64_t i = 3; i <= std::sqrt(n); i += 2)
    {
        while (n > 2 && n % i == 0)
        {
            ret.emplace_back(i);
            n /= i;
        }
    }

    //If we are left with a number other than 1 (and 2 is already guaranteed gone)
    //The remaining number is prime
    //We may hit this because the sqrt(n) shrinks as we go along
    if (n > 2)
    {
        ret.emplace_back(n);
    }

    return ret;

}
