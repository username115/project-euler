
#include <iostream>
#include "implementation.hpp"
#include <algorithm>


int main(int argc, char* argv[])
{
    std::uint64_t numToFactor = 600851475143ull;

    auto primes = getPrimeFactors(numToFactor);

    for (const auto& p : primes)
    {
        std::cout << p << std::endl;
    }

    std::cout << std::endl;

    std::sort(primes.begin(), primes.end());
    std::cout << *(primes.end()-1) << std::endl;

    return 0;

}