
#include "implementation.hpp"
#include <gtest/gtest.h>

#include <algorithm>

TEST(primes, seven)
{
    auto primes = getPrimeFactors(7);
    ASSERT_EQ(1, primes.size());
    ASSERT_EQ(7, primes[0]);
}

TEST(primes, square)
{
    auto primes = getPrimeFactors(25);
    ASSERT_EQ(2, primes.size());
    ASSERT_EQ(5, primes[0]);
    ASSERT_EQ(5, primes[1]);
}

TEST(primes, largenum)
{
    auto primes = getPrimeFactors(45894234872);
    ASSERT_EQ(5, primes.size());
    std::sort(primes.begin(), primes.end());
    ASSERT_EQ(2, primes[0]);
    ASSERT_EQ(2, primes[1]);
    ASSERT_EQ(2, primes[2]);
    ASSERT_EQ(379, primes[3]);
    ASSERT_EQ(15136621, primes[4]);
}

int main(int argc, char* argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
