
package problem1;


fun main() {
    var sum = 0;

    for (i in 1..999) { //the version of Kotlin I have doesn't seem to support the 1..<1000 syntax for ranges
        if (i % 3 == 0 || i % 5 == 0) {
            sum = sum + i;
        }
    }

    print(sum);
}