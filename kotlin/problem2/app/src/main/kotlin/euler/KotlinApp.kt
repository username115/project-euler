
package euler;


fun main() {
    var num1 = 0;
    var num2 = 1;
    var next = 1;

    var sum = 0;

    while (next < 4_000_000) {
        if ((next and 1) == 0) {
            sum += next;
        }
        next = num1 + num2;
        num1 = num2;
        num2 = next;
    }

    print(sum);
}