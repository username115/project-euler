

num1 = 0
num2 = 1
next = 1

sum = 0

while next <= 4_000_000:
    if not next & 1:
        sum = sum + next
    next = num1 + num2
    num1 = num2
    num2 = next

print(sum)