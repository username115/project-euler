fn main() {
    let mut num1 = 0;
    let mut num2 = 1;
    let mut next = 1;

    let mut sum = 0;

    while next < 4_000_000 {
        if (next & 1) == 0 {
            sum = sum + next;
        }
        next = num1 + num2;
        num1 = num2;
        num2 = next;
    }

    println!("{}", sum)
    
}
