

pub fn get_prime_factors(n: i64) -> Vec<i64> {
    let mut ret: Vec<i64> = Vec::new();
    let mut temp = n;


    while temp % 2 == 0 {
        ret.push(2);
        temp /= 2;
    }

    let mut i: i64 = 3;
    while i <= (temp as f64).sqrt() as i64 {
        while temp > 2 && temp % i == 0 {
            ret.push(i);
            temp /= i;
        }

        i += 1;
    }

    if temp > 2 {
        ret.push(temp);
    }

    return ret;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_seven() {
        let primes = get_prime_factors(7);

        assert_eq!(1, primes.len());
        assert_eq!(7, primes[0]);
    }

    #[test]
    fn test_square() {
        let primes = get_prime_factors(25);
        
        assert_eq!(2, primes.len());
        assert_eq!(5, primes[0]);
        assert_eq!(5, primes[1]);
    }

    #[test]
    fn test_largenum() {
        let mut primes = get_prime_factors(45894234872);

        assert_eq!(5, primes.len());
        primes.sort();
        assert_eq!(2, primes[0]);
        assert_eq!(2, primes[1]);
        assert_eq!(2, primes[2]);
        assert_eq!(379, primes[3]);
        assert_eq!(15136621, primes[4]);
    }
}

