
mod implementation;
use implementation::get_prime_factors;

fn main() {
    
    let num_to_factor: i64 = 600_851_475_143;

    let mut primes = get_prime_factors(num_to_factor);
    

    for p in &primes {
        println!("{}", p);
    }

    println!("");
    primes.sort();
    println!("{}", primes.last().unwrap());

}
